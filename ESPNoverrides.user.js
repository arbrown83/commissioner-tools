// ==UserScript==
// @name           ESPN Fantasy Football 
// @description    Overrides for ESPN FF pages
// @author         Andy Brown
// @include        http://games.espn.go.com/ffl/*
// @version        1.0
// ==/UserScript==

var ads = document.getElementsByClassName("games-footercol");
var ads2 = document.getElementsByClassName("games-ad300");
var spacing1 = document.getElementsByClassName("games-rightcol-spacer");
var sideBlocks = document.getElementsByClassName("lo-sidebar-box");
var columnist = document.getElementsByClassName("columnist-parent");

// get the team id from the URL
// alert( document.URL );

// remove ad at the bottom of the page
for (i=0; i<ads.length; i++)
{
	ads[i].style.visibility = 'hidden';
}

// remove ad at the top of right sidebar
for (i=0; i<ads2.length; i++)
{
	ads2[i].style.visibility = 'hidden';
}

// remove spacing section in right sidebar, where ad used to be
for (i=0; i<spacing1.length; i++)
{
	spacing1[i].style.height = '0px';
}

// remove columnist items from block
for (i=0; i<columnist.length; i++)
{
    columnist[i].parentNode.style.height = '100%';
    columnist[i].parentNode.innerHTML = '<div>'+
    '<h1>Featured Matchup</h1>'+
    '<iframe src="http://ar-brown.net/parser/example/featuredmatchup.php" style="height: auto;" width="260px"></iframe>'+
    '</div>';
    //columnist[i].parentNode.style.visibility = 'hidden';
    //columnist[i].parentNode.style.margin-bottom = '0px';
    
}

/* mess with the width of the site (in a table? WTF?)
var width = document.getElementsByClassName("maincontainertbl");

for (i=0; i<width.length; i++)
{
	width[i].style.width= '100%';
}
*/